FROM rust:1.80

ARG TARGETARCH
ARG BITCOIND_VERSION=27.0

RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive \
    apt-get install --no-install-recommends --assume-yes \
      protobuf-compiler python3 python3-pip

# Select the correct version of bitcoind based on the platform
RUN BITCOIND_ARCH=$([ "$TARGETARCH" = "arm64" ] && echo "aarch64" || echo "x86_64") && \
    wget --no-verbose https://bitcoin.org/bin/bitcoin-core-$BITCOIND_VERSION/bitcoin-$BITCOIND_VERSION-$BITCOIND_ARCH-linux-gnu.tar.gz && \
    tar xzf bitcoin-$BITCOIND_VERSION-$BITCOIND_ARCH-linux-gnu.tar.gz && \
    mv bitcoin-$BITCOIND_VERSION/bin/bitcoind /usr/local/bin && \
    rm -rf bitcoin-$BITCOIND_VERSION

WORKDIR /usr/src/lnrod

# Install vsld2
COPY Cargo* .
COPY scripts scripts/
RUN cargo install vls-proxy --version=`./scripts/vls-revision` --bin=vlsd2 --profile=dev

# Install the Python dependencies
COPY requirements.txt .
RUN pip3 install --break-system-packages -r requirements.txt

# Build lnrod
COPY build.rs .
COPY src src/
RUN cargo build

# Compile the proto files into the `tests` dir
COPY src/admin/admin.proto src/admin/
COPY tests tests/
RUN ./scripts/compile-proto

# TODO: fix the nulltransport tests
ENV SIGNER=vls2-grpc
ENTRYPOINT ["./tests/integration-test.py"]
